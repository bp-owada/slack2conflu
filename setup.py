from setuptools import find_packages, setup

setup(
    name='src',
    packages=find_packages(),
    version='0.1.0',
    description='extract "IINE" contents from Slack and send to Confluence',
    author='satoshi.ohwada',
    license='MIT',
)
